# The packaging information for python-consul in XiVO

This repository contains the packaging information for
[python-threadpool](http://chrisarndt.de/projects/threadpool/).

To get a new version of python-threadpool in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.
